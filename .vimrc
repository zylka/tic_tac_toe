" file: .vimrc
" local vimrc files with cpp components

" file suffixes
augroup project
	autocmd!
	autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

" set path variable
let &path.="src/include,/usr/includeAl,"
