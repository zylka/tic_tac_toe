/*!
 * @file config.hpp
 * @date September 2023
 * @author Adam Zylka
 */


#pragma once

/*!
 * @var ROWS
 * Number of rows
 */
const int ROWS = 3;

/*!
 * @var COLS
 * Number of cols
 */
const int COLS = 3;


/*!
 * @fn showConfig
 * Prints the configuration.
 * @param None
 * @return None
 */
extern void showConfig(void);

