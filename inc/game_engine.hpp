/*!
 * @file game_engine.hpp
 * @date September 2023
 * @author Adam Zylka
 */


#pragma once


/*!
 * @fn runGame
 * Execute game.
 * @param None
 * @return None
 */
extern void runGame(void);
