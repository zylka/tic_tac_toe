/*!
 * @file show_help.hpp
 * @date September 2023
 * @author Adam Zylka
 */


#pragma once


/*!
 * @fn showHelp
 * Prints the instruction, how to run the game.
 * @param None
 * @return None
 */
extern void showHelp(void);
