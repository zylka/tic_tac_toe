/*!
 * @file config.cpp
 * @date September 2023
 * @author Adam Zylka
 */


#include "config.hpp"
#include <iostream>


void showConfig(void)
{
  std::cout << "===========\n";
  std::cout << "TIC TAC TOE\n";
  std::cout << "===========\n\n";
  std::cout << "CONFIG\n";
  std::cout << " - rows: " << ROWS << std::endl;
  std::cout << " - cols: " << COLS << std::endl;
}
