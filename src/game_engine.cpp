/*!
 * @file game_engine.cpp
 * @date September 2023
 * @author Adam Zylka
 */

#include "game_engine.hpp"
#include "config.hpp"

#include <iostream>
#include <sstream>
#include <string.h>
#include <vector>

using namespace std;

static string player0 = "O";
static string playerX = "X";


static void showInstruction(string player)
{
  cout << "Player '" << player << "' insert two numbers separated by spaces: "
      << "1 <= row <= " << ROWS << " ; 1 <= col <= " << COLS
      << " or q for quit\n";
}


static void getInput(string player, vector<string> *input)
{
  string line;
  bool inputOk = false;

  while (!inputOk)
  {
    cout << "Player '" << player << "' input: ";
    getline(cin, line);

    switch (line.size())
    {
      case 1:
      {
        if ((strcmp((const char *)&line[0], (const char *)"q") == 0)
            || (strcmp((const char *)&line[0], (const char *)"Q") == 0))
        {
          input->push_back("q");
          inputOk = true;
        }
        break;
      }

      case 3:
      {
        string sRow, sCol;
        sRow = line[0];
        sCol = line[2];
        int row = stoi(sRow);
        int col = stoi(sCol);

        if (1 <= row && row <= ROWS && 1 <= col && col <= COLS)
        {
          input->push_back(sRow);
          input->push_back(sCol);
          inputOk = true;
        }
      }
    }

    if (!inputOk)
    {
      cout << "Error: wrong input! Try again...\n";
    }

  }
}


static bool checkInputNok(vector<vector<string>> *fields, vector<string> *input)
{
  bool ret;

  int row = stoi(input->at(0));
  int col = stoi(input->at(1));


  if (strcmp((const char *)fields->at(row-1)[col-1].c_str(), (const char *)" ") == 0)
    ret = false;
  else
  {
    ret = true;
  }

  return ret;
}


void updateFields(string player, vector<vector<string>> *fields, vector<string> *input)
{
  int row = stoi(input->at(0));
  int col = stoi(input->at(1));

  fields->at(row-1)[col-1] = player;  // adapt index starting from 0
}


void createBoard(vector<vector<string>> *board)
{
  int cellRows = 3, cellCols = 3;
  vector<string> line;
  board->clear();

  for (int cellRow = 0; cellRow < cellRows; cellRow++)
  {
    // create a new line
    line.clear();
    for (int coll = 0; coll < COLS; coll++)
    {
      for (int cellCol = 0; cellCol < cellCols; cellCol++)
      {
        line.push_back(" ");
      }
      if (coll < COLS - 1)
        line.push_back("|");
    }
    board->push_back(line);

    // add ----- between the lines
    if (cellRow < ROWS -1)
    {
      int lineLen = line.size();
      line.clear();
      for (int i = 0; i < lineLen; i++)
        line.push_back("-");
      board->push_back(line);
    }
  }
}

void showBoard(vector<vector<string>> *board, vector<vector<string>> *fields)
{
  int yPrint;

  for (int y = 0; y < board->size(); y++)
  {
    for (int x = 0; x < board->at(y).size(); x++)
    {
      if (y % 2 == 0)
      {
        switch (y)
        {
          case 0: yPrint = 0; break;
          case 2: yPrint = 1; break;
          case 4: yPrint = 2; break;
        }
        switch (x)
        {
          case 1: cout << fields->at(yPrint)[0]; break;
          case 5: cout << fields->at(yPrint)[1]; break;
          case 9: cout << fields->at(yPrint)[2]; break;
          default: cout << board->at(y)[x]; // print spaces/separators in line
        }
      }
      else  // print ---
      {
        cout << board->at(y)[x];
      }
    }
    cout << endl;
  }
}

void changePlayer(string *player)
{
  if (*player == player0)
    *player = playerX;
  else if (*player == playerX)
    *player = player0;
}


void runGame(void)
{
  cout << "Starting the game\n";

  string player = player0;
  vector<string> input;
  bool runGame = true;
  vector<vector<string>> fields(ROWS, vector<string> (COLS, " "));
  vector<vector<string>> board;
  bool inputNok = true;

  createBoard(&board);

  while (runGame)
  {
    while (inputNok)
    {
      input.clear();  // empty input

      showInstruction(player);
      getInput(player, &input);

      // check if exit game
      if (input.size() == 1 && input[0] == "q")
      {
        cout << "exit game\n";
        runGame = false;
        break;  // exit loop
      }

      inputNok = checkInputNok(&fields, &input);
      if (inputNok)
      {
        cout << "Player '" << player << "' field is already occupied! Chose another field\n";
        showBoard(&board, &fields);
      }
    }

    if (!runGame) // exit outer loop
    {
      break;
    }

    updateFields(player, &fields, &input);
    showBoard(&board, &fields);
    changePlayer(&player);

    inputNok = true;
  }
}

