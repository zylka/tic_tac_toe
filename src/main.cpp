/*!
 * @file main.cpp
 * @date September 2023
 * @author Adam Zylka
 */

#include <iostream>
#include <string.h>

#include "config.hpp"
#include "game_engine.hpp"
#include "show_help.hpp"

using namespace std;


/*!
 * @fn processArgs
 * Iterates over all additional arguments and prints the corresponding output.
 * @param argc: number of arguments
 * @param **argv: actual arguments
 * @return None
 */
void processArgs(int argc, char **argv)
{
  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "--config") == 0)
      showConfig();
    else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
      showHelp();
    else
      showHelp();
  }
}


int main(int argc, char **argv)
{
  if (argc > 1)
    processArgs(argc, argv);
  else
  {
    runGame();
  }

  return 0;
}
