/*!
 * @file show_help.cpp
 * @date September 2023
 * @author Adam Zylka
 */


#include "show_help.hpp"
#include <iostream>


void showHelp(void)
{
  std::string projectName = "tic_tac_toe";

  std::cout << "===========\n";
  std::cout << "TIC TAC TOE\n";
  std::cout << "===========\n\n";
  std::cout << "HELP\n";
  std::cout << " - how to run\n";
  std::cout << "   => ./" << projectName << std::endl;
  std::cout << " - show help\n";
  std::cout << "   => ./" << projectName << " -h / --help\n";
  std::cout << " - show config\n";
  std::cout << "   => ./" << projectName << " -c / --config\n";
}
